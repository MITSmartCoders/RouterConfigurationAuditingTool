from tkinter import colorchooser
from tkinter import filedialog
from tkinter import *
import sys
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True

import RouterConfig_support_FileChooser

def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = tk.Tk()
    top = Toplevel1 (root)
    RouterConfig_support_FileChooser.init(root, top)
    root.mainloop()

w = None
def create_Toplevel1(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = tk.Toplevel (root)
    top = Toplevel1 (w)
    RouterConfig_support_FileChooser.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_Toplevel1():
    global w
    w.destroy()
    w = None

class Toplevel1:
    def mfileopen(self):
	    file1=filedialog.askopenfile()
	    self.label=file1 
	    print(self.label.name)
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85'
        _ana2color = '#ececec' # Closest X11 color: 'gray92'

        top.geometry("760x450+287+190")
        top.minsize(120, 1)
        top.maxsize(1370, 749)
        top.resizable(1, 1)
        top.title("New Toplevel")
        top.configure(background="#d9d9d9")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="black")

        self.Frame1 = tk.Frame(top)
        self.Frame1.place(relx=0.013, rely=-0.022, relheight=1.167
                , relwidth=0.993)
        self.Frame1.configure(relief='groove')
        self.Frame1.configure(borderwidth="2")
        self.Frame1.configure(relief="groove")
        self.Frame1.configure(background="#d9d9d9")
        self.Frame1.configure(highlightbackground="#d9d9d9")
        self.Frame1.configure(highlightcolor="black")

        self.Label1 = tk.Label(self.Frame1)
        self.Label1.place(relx=0.212, rely=0.057, height=51, width=344)
        self.Label1.configure(activebackground="#f9f9f9")
        self.Label1.configure(activeforeground="black")
        self.Label1.configure(background="#c0c0c0")
        self.Label1.configure(disabledforeground="#400040")
        self.Label1.configure(font="-family {Segoe UI} -size 14")
        self.Label1.configure(foreground="#8080ff")
        self.Label1.configure(highlightbackground="#d9d9d9")
        self.Label1.configure(highlightcolor="black")
        self.Label1.configure(text='''Router Configration auditing Tool''')

        self.Frame2 = tk.Frame(self.Frame1)
        self.Frame2.place(relx=0.212, rely=0.171, relheight=0.181
                , relwidth=0.483)
        self.Frame2.configure(relief='groove')
        self.Frame2.configure(borderwidth="2")
        self.Frame2.configure(relief="groove")
        self.Frame2.configure(background="#d9d9d9")
        self.Frame2.configure(highlightbackground="#d9d9d9")
        self.Frame2.configure(highlightcolor="black")

        self.Browse = tk.Button(self.Frame2)
        self.Browse.place(relx=0.603, rely=0.421, height=34, width=107)
        self.Browse.configure(activebackground="#ececec")
        self.Browse.configure(activeforeground="#000000")
        self.Browse.configure(background="#d9d9d9")
        self.Browse.configure(disabledforeground="#a3a3a3")
        self.Browse.configure(foreground="#000000")
        self.Browse.configure(highlightbackground="#d9d9d9")
        self.Browse.configure(highlightcolor="black")
        self.Browse.configure(pady="0")
        self.Browse.configure(text='''Browse''',command=self.mfileopen)

        self.browse = tk.Entry(self.Frame2)
        self.browse.place(relx=0.137, rely=0.421,height=30, relwidth=0.449)
        self.browse.configure(background="white")
        self.browse.configure(disabledforeground="#a3a3a3")
        self.browse.configure(font="TkFixedFont")
        self.browse.configure(foreground="#000000")
        self.browse.configure(insertbackground="black",text="")

        self.show = tk.Button(self.Frame1)
        self.show.place(relx=0.45, rely=0.362, height=54, width=97)
        self.show.configure(activebackground="#ececec")
        self.show.configure(activeforeground="#000000")
        self.show.configure(background="#d9d9d9")
        self.show.configure(disabledforeground="#a3a3a3")
        self.show.configure(foreground="#000000")
        self.show.configure(highlightbackground="#d9d9d9")
        self.show.configure(highlightcolor="black")
        self.show.configure(overrelief="raised")
        self.show.configure(pady="0")
        self.show.configure(text='''Show Config''',command=self.desplayPath)


    def desplayPath(self):
        from ciscoconfparse import CiscoConfParse
        from ciscoconfparse.ccp_util import IPv4Obj
        print(self.label.name)
        parse = CiscoConfParse(self.label.name, syntax='ios')
        print(parse .find_children("^line console 1 4"))
        if __name__ == "__main__":
            confparse = CiscoConfParse(self.label.name)
            ospf_regex_pattern = r"^router ospf \d+$"
            # in this case, we will simply check that the ospf router command is part of the config
            is_ospf_in_use = confparse.has_line_with(ospf_regex_pattern)
            if is_ospf_in_use:
                print("OSPF is used in this configuration")
                # result["features"].append("ospf")
            else:
                print(" OSPF is not used in this configuration")
        self.listBox = tk.Listbox(self.Frame1)
        self.listBox.place(relx=0.146, rely=0.476, relheight=0.366 , relwidth=0.721)
        self.listBox.configure(background="white")
        self.listBox.configure(disabledforeground="#a3a3a3")
        self.listBox.configure(font="TkFixedFont")
        self.listBox.configure(foreground="#000000")        
        for intf_obj in parse.find_objects_w_child('^interface', '^\s+shutdown'):
            print("Shutdown1: " + intf_obj.text)
            self.listBox.insert(END ,"Shutdown: " + intf_obj.text)

        self.listBox = tk.Listbox(self.Frame1)
        self.listBox.place(relx=0.146, rely=0.476, relheight=0.366 , relwidth=0.721)
        self.listBox.configure(background="white")
        self.listBox.configure(disabledforeground="#a3a3a3")
        self.listBox.configure(font="TkFixedFont")
        self.listBox.configure(foreground="#000000")     
        parse = CiscoConfParse(self.label.name, syntax='ios')
        for intf_obj in parse.find_objects('^interface'):
            intf_name = intf_obj.re_match_typed('^interface\s+(\S.+?)$')
            intf_ip_addr = intf_obj.re_match_iter_typed(
                r'ip\saddress\s(\d+\.\d+\.\d+\.\d+)\s', result_type=str,
                group=1, default='Shutdawn')
            print("{0}: {1}".format(intf_name, intf_ip_addr))
            self.listBox.insert(END ,"{0}: {1}".format(intf_name, intf_ip_addr))

         	

if __name__ == '__main__':
    vp_start_gui()





