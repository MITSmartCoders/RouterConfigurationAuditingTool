import sys
from tkinter import messagebox
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True
import routerConfig_support

from netmiko import ConnectHandler
from ciscoconfparse import CiscoConfParse
from ciscoconfparse import CiscoPassword
import os
from tkinter import BOTH, END, LEFT
import routerConfig_support
def vp_start_gui1():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = tk.Tk()
    top = Toplevel2 (root)
    routerConfig_support.init(root, top)
    root.mainloop()
w = None

class Toplevel2:
    def showConfig(self):
        parse = CiscoConfParse("config.txt", syntax='ios')
        print(parse .find_children("^line console 1 4"))
        if __name__ == "__main__":
            confparse = CiscoConfParse("config.txt")
            ospf_regex_pattern = r"^router ospf \d+$"
            is_ospf_in_use = confparse.has_line_with(ospf_regex_pattern)
            if is_ospf_in_use:
                print("OSPF is used in this configuration")
            else:
                print(" OSPF is not used in this configuration")
        self.listBox = tk.Listbox(self.Frame1)
        self.listBox.place(relx=0.146, rely=0.1, relheight=0.366 , relwidth=0.721)
        self.listBox.configure(background="white")
        self.listBox.configure(disabledforeground="#a3a3a3")
        self.listBox.configure(font="TkFixedFont")
        self.listBox.configure(foreground="#000000")        
        confparse = CiscoConfParse("config.txt")
        ospf_regex_pattern = r"^router ospf \d+$"
        rip_regex_pattern = r"^router rip \d+$"
        bgp_regex_pattern = r"^router bgp \d+$"

        is_ospf_in_use = confparse.has_line_with(ospf_regex_pattern)
        is_rip_in_use = confparse.has_line_with(rip_regex_pattern)
        is_bgp_in_use = confparse.has_line_with(bgp_regex_pattern)
        if is_ospf_in_use:
            print("OSPF is used in this configuration")
            self.listBox.insert(END ,"OSPF is used in this configuration")
        elif is_rip_in_use :
            print("RIP is used in this configuration")
            self.listBox.insert(END ,"RIP is used in this configuration")
        elif is_bgp_in_use :
            print("BGP is used in this configuration")
            self.listBox.insert(END ,"BGP is used in this configuration")
        else:
            print(" it has't used in any configuration")

        self.listBox = tk.Listbox(self.Frame1)
        self.listBox.place(relx=0.146, rely=0.476, relheight=0.366 , relwidth=0.721)
        self.listBox.configure(background="white")
        self.listBox.configure(disabledforeground="#a3a3a3")
        self.listBox.configure(font="TkFixedFont")
        self.listBox.configure(foreground="#000000")     
        parse = CiscoConfParse("config.txt", syntax='ios')
        for intf_obj in parse.find_objects('^interface'):
            intf_name = intf_obj.re_match_typed('^interface\s+(\S.+?)$')
            intf_ip_addr = intf_obj.re_match_iter_typed(
                r'ip\saddress\s(\d+\.\d+\.\d+\.\d+)\s', result_type=str,
                group=1, default='Shutdawn')
            print("{0}: {1}".format(intf_name, intf_ip_addr))
            self.listBox.insert(END ,"{0}: {1}".format(intf_name, intf_ip_addr))

        #Deleting file that has been created
        if os.path.exists("config.txt"):
          os.remove("config.txt")
        else:
          print("The file does not exist")


    def __init__(self, top=None):
        
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85'
        _ana2color = '#ececec' # Closest X11 color: 'gray92'

        top.geometry("686x499+310+128")
        top.minsize(120, 1)
        top.maxsize(1370, 749)
        top.resizable(1, 1)
        top.title("New Toplevel")
        top.configure(background="#d9d9d9")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="black")

        self.Frame1 = tk.Frame(top)
        self.Frame1.place(relx=0.044, rely=0.02, relheight=0.932, relwidth=0.94)
        self.Frame1.configure(relief='groove')
        self.Frame1.configure(borderwidth="2")
        self.Frame1.configure(relief="groove")
        self.Frame1.configure(background="#d9d9d9")
        self.Frame1.configure(highlightbackground="#d9d9d9")
        self.Frame1.configure(highlightcolor="black")


        self.Button2 = tk.Button(self.Frame1)
        self.Button2.place(relx=0.496, rely=0.1, height=42, width=80)
        self.Button2.configure(activebackground="#ececec")
        self.Button2.configure(activeforeground="#000000")
        self.Button2.configure(background="#d9d9d9")
        self.Button2.configure(disabledforeground="#a3a3a3")
        self.Button2.configure(font="-family {Segoe UI} -size 20")
        self.Button2.configure(foreground="#000000")
        self.Button2.configure(highlightbackground="#d9d9d9")
        self.Button2.configure(highlightcolor="black")
        self.Button2.configure(pady="0")
        self.Button2.configure(text='''show config''')
        self.Button2.configure(command=self.showConfig)



        self.Label5 = tk.Label(self.Frame1)
        self.Label5.place(relx=0.186, rely=-0.022, height=43, width=239)
        self.Label5.configure(activebackground="#f9f9f9")
        self.Label5.configure(activeforeground="black")
        self.Label5.configure(background="#ff7dbe")
        self.Label5.configure(disabledforeground="#a3a3a3")
        self.Label5.configure(font="-family {Segoe UI} -size 20 -underline 1")
        self.Label5.configure(foreground="#000000")
        self.Label5.configure(highlightbackground="#d9d9d9")
        self.Label5.configure(highlightcolor="black")
        self.Label5.configure(takefocus="70")
        self.Label5.configure(text='''Router Auditer Tool''')
