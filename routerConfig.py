import sys
from tkinter import messagebox
try:
    import Tkinter as tk
except ImportError:
    import tkinter as tk

try:
    import ttk
    py3 = False
except ImportError:
    import tkinter.ttk as ttk
    py3 = True
import routerConfig_support

from netmiko import ConnectHandler
import os
from ciscoconfparse import CiscoPassword
from Read_Configuration import Toplevel2
def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = tk.Tk()
    top = Toplevel1 (root)
    routerConfig_support.init(root, top)
    root.mainloop()

w = None
def create_Toplevel1(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = tk.Toplevel (root)
    top = Toplevel1 (w)
    routerConfig_support.init(w, top, *args, **kwargs)
    return (w, top)

def destroy_Toplevel1():
    global w
    w.destroy()
    w = None

class Toplevel1:
    def close(delf):
        msg=messagebox.askyesno("log in page","are want to close the page?")
        if(msg):
            exit()
        else:
            print("Error!")

    def new_window(self, top=None):
        global val, w, root
        ip=self.host_ip.get()
        hostname=self.hostname.get()
        password=self.password.get()
        passwordsecret=self.secret_text.get()
        
        iosv_l2 = {
         'device_type':'cisco_ios_telnet',
         'ip': ip, 
         'username': hostname, 
         'password': password, 
         'secret':passwordsecret
        } 
        conn = ConnectHandler(**iosv_l2)
        if(conn):
            conn.enable()
            a=conn.send_command("show run")
            # print(a)
            f = open("config.txt", "a")
            f.write(a)
            f.close()
            msg=messagebox.showinfo("login successfull",'login successfull')
            top = Toplevel2 (root)
            routerConfig_support.init(root, top)
            root.mainloop()

        else:
            msg=messagebox.askyesno("some thing wrong")

    def __init__(self, top=None):
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#d9d9d9' # X11 color: 'gray85'
        _ana1color = '#d9d9d9' # X11 color: 'gray85'
        _ana2color = '#ececec' # Closest X11 color: 'gray92'

        top.geometry("686x499+310+128")
        top.minsize(120, 1)
        top.maxsize(1370, 749)
        top.resizable(1, 1)
        top.title("New Toplevel")
        top.configure(background="#d9d9d9")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="black")

        self.Frame1 = tk.Frame(top)
        self.Frame1.place(relx=0.044, rely=0.02, relheight=0.932, relwidth=0.94)
        self.Frame1.configure(relief='groove')
        self.Frame1.configure(borderwidth="2")
        self.Frame1.configure(relief="groove")
        self.Frame1.configure(background="#d9d9d9")
        self.Frame1.configure(highlightbackground="#d9d9d9")
        self.Frame1.configure(highlightcolor="black")

        self.Label1 = tk.Label(self.Frame1)
        self.Label1.place(relx=0.155, rely=0.151, height=31, width=63)
        self.Label1.configure(activebackground="#f9f9f9")
        self.Label1.configure(activeforeground="black")
        self.Label1.configure(background="#d9d9d9")
        self.Label1.configure(disabledforeground="#a3a3a3")
        self.Label1.configure(font="-family {Segoe UI} -size 14")
        self.Label1.configure(foreground="#000000")
        self.Label1.configure(highlightbackground="#d9d9d9")
        self.Label1.configure(highlightcolor="black")
        self.Label1.configure(state='active')
        self.Label1.configure(text='''host ip''')

        self.Label2 = tk.Label(self.Frame1)
        self.Label2.place(relx=0.155, rely=0.28, height=21, width=94)
        self.Label2.configure(activebackground="#f9f9f9")
        self.Label2.configure(activeforeground="black")
        self.Label2.configure(background="#d9d9d9")
        self.Label2.configure(disabledforeground="#a3a3a3")
        self.Label2.configure(font="-family {Segoe UI} -size 14")
        self.Label2.configure(foreground="#000000")
        self.Label2.configure(highlightbackground="#d9d9d9")
        self.Label2.configure(highlightcolor="black")
        self.Label2.configure(state='active')
        self.Label2.configure(text='''hostname''')

        self.Label3 = tk.Label(self.Frame1)
        self.Label3.place(relx=0.14, rely=0.387, height=31, width=86)
        self.Label3.configure(activebackground="#f9f9f9")
        self.Label3.configure(activeforeground="black")
        self.Label3.configure(background="#d9d9d9")
        self.Label3.configure(disabledforeground="#a3a3a3")
        self.Label3.configure(font="-family {Segoe UI} -size 14")
        self.Label3.configure(foreground="#000000")
        self.Label3.configure(highlightbackground="#d9d9d9")
        self.Label3.configure(highlightcolor="black")
        self.Label3.configure(state='active')
        self.Label3.configure(text='''password''')

        self.Label4 = tk.Label(self.Frame1)
        self.Label4.place(relx=0.14, rely=0.516, height=31, width=141)
        self.Label4.configure(activebackground="#f9f9f9")
        self.Label4.configure(activeforeground="black")
        self.Label4.configure(background="#d9d9d9")
        self.Label4.configure(disabledforeground="#a3a3a3")
        self.Label4.configure(font="-family {Segoe UI} -size 14")
        self.Label4.configure(foreground="#000000")
        self.Label4.configure(highlightbackground="#d9d9d9")
        self.Label4.configure(highlightcolor="black")
        self.Label4.configure(state='active')
        self.Label4.configure(text='''secret password''')

        self.host_ip = tk.Entry(self.Frame1)
        self.host_ip.place(relx=0.45, rely=0.108,height=40, relwidth=0.347)
        self.host_ip.configure(background="white")
        self.host_ip.configure(disabledforeground="#a3a3a3")
        self.host_ip.configure(font="TkFixedFont")
        self.host_ip.configure(foreground="#000000")
        self.host_ip.configure(highlightbackground="#d9d9d9")
        self.host_ip.configure(highlightcolor="black")
        self.host_ip.configure(insertbackground="black")
        self.host_ip.configure(selectbackground="#c4c4c4")
        self.host_ip.configure(selectforeground="black")

        self.hostname = tk.Entry(self.Frame1)
        self.hostname.place(relx=0.45, rely=0.237,height=40, relwidth=0.347)
        self.hostname.configure(background="white")
        self.hostname.configure(disabledforeground="#a3a3a3")
        self.hostname.configure(font="TkFixedFont")
        self.hostname.configure(foreground="#000000")
        self.hostname.configure(highlightbackground="#d9d9d9")
        self.hostname.configure(highlightcolor="black")
        self.hostname.configure(insertbackground="black")
        self.hostname.configure(selectbackground="#c4c4c4")
        self.hostname.configure(selectforeground="black")

        self.password = tk.Entry(self.Frame1)
        self.password.place(relx=0.45, rely=0.366,height=40, relwidth=0.347)
        self.password.configure(show="*")
        self.password.configure(disabledforeground="#a3a3a3")
        self.password.configure(font="TkFixedFont")
        self.password.configure(foreground="#000000")
        self.password.configure(highlightbackground="#d9d9d9")
        self.password.configure(highlightcolor="black")
        self.password.configure(insertbackground="black")
        self.password.configure(selectbackground="#c4c4c4")
        self.password.configure(selectforeground="black")

        self.secret_text = tk.Entry(self.Frame1)
        self.secret_text.place(relx=0.45, rely=0.495,height=40, relwidth=0.347)
        self.secret_text.configure(show="*")
        self.secret_text.configure(disabledforeground="#a3a3a3")
        self.secret_text.configure(font="TkFixedFont")
        self.secret_text.configure(foreground="#000000")
        self.secret_text.configure(highlightbackground="#d9d9d9")
        self.secret_text.configure(highlightcolor="black")
        self.secret_text.configure(insertbackground="black")
        self.secret_text.configure(selectbackground="#c4c4c4")
        val1=self.secret_text.configure(selectforeground="black")


        self.Button1 = tk.Button(self.Frame1)
        self.Button1.place(relx=0.31, rely=0.71, height=42, width=60)
        self.Button1.configure(activebackground="#ececec")
        self.Button1.configure(activeforeground="#000000")
        self.Button1.configure(background="#d9d9d9")
        self.Button1.configure(disabledforeground="#a3a3a3")
        self.Button1.configure(font="-family {Segoe UI} -size 14")
        self.Button1.configure(foreground="#000000")
        self.Button1.configure(highlightbackground="#d9d9d9")
        self.Button1.configure(highlightcolor="black")
        self.Button1.configure(pady="0")
        self.Button1.configure(text='''login''')
        self.Button1.configure(command=self.new_window)



        self.Button2 = tk.Button(self.Frame1)
        self.Button2.place(relx=0.496, rely=0.71, height=42, width=60)
        self.Button2.configure(activebackground="#ececec")
        self.Button2.configure(activeforeground="#000000")
        self.Button2.configure(background="#d9d9d9")
        self.Button2.configure(disabledforeground="#a3a3a3")
        self.Button2.configure(font="-family {Segoe UI} -size 14")
        self.Button2.configure(foreground="#000000")
        self.Button2.configure(highlightbackground="#d9d9d9")
        self.Button2.configure(highlightcolor="black")
        self.Button2.configure(pady="0")
        self.Button2.configure(text='''close''')
        self.Button2.configure(command=self.close)



        self.Label5 = tk.Label(self.Frame1)
        self.Label5.place(relx=0.186, rely=-0.022, height=43, width=239)
        self.Label5.configure(activebackground="#f9f9f9")
        self.Label5.configure(activeforeground="black")
        self.Label5.configure(background="#ff7dba")
        self.Label5.configure(disabledforeground="#a3a3a3")
        self.Label5.configure(font="-family {Segoe UI} -size 20 -underline 1")
        self.Label5.configure(foreground="#000000")
        self.Label5.configure(highlightbackground="#d9d9d9")
        self.Label5.configure(highlightcolor="black")
        self.Label5.configure(takefocus="70")
        self.Label5.configure(text='''Router Auditer Tool''')
        root.mainloop()   
   
if __name__ == '__main__':
    vp_start_gui()





